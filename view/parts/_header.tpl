<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" href="/favicon.png">

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="/assets/css/fa-solid.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">

    <title>{$title}</title>
</head>
<body>

{if $isLogin}
<div class="login">
    <div class="login__exp">
        Session exp: <span class="login__exp-counter" data-exp="{$smarty.session.isLoginExp-$smarty.now}">{$smarty.session.isLoginExp-$smarty.now}</span> sec
    </div>
    <a href="/index.php?do=logout" class="login__logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
    <div class="clearfix"></div>
</div>
{/if}