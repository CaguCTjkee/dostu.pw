<tr class="password__item">
    <td>
        <input type="text" name="title[]" class="form-control password__input"
               {if !empty($row.title)}value="{$row.title}"{/if}>
    </td>
    <td>
        <input type="text" name="link[]" class="form-control password__input"
               {if !empty($row.link)}value="{$row.link}"{/if}>
    </td>
    <td>
        <input type="text" name="login[]" class="form-control password__input"
               {if !empty($row.login)}value="{$row.login}"{/if}>
    </td>
    <td>
        <input type="text" name="password[]" class="form-control password__input password__input_protect"
               {if !empty($row.password)}value="{$row.password}"{/if}>
    </td>
    <td>
        <input type="text" name="description[]" class="form-control password__input"
               {if !empty($row.description)}value="{$row.description}"{/if}>
        <div class="password__tools">
            <a href="#" class="password__move">
                <i class="fas fa-bars password__move-icon"></i>
            </a>
            <a href="#" class="password__remove">
                <i class="fas fa-trash-alt password__remove-icon"></i>
            </a>
        </div>
    </td>
</tr>