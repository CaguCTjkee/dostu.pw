{include 'parts/_header.tpl'}

<div class="container mt-5">
    <div class="row">
        <div class="col-12">
            <form action="index.php?" method="post" class="password">
                <div class="password__loading">
                    <i class="fas fa-spinner password__spinner"></i>
                </div>
                <table class="table table-bordered password__table">
                    <thead>
                    <tr>
                        <th>Тип / Наименование</th>
                        <th>Ссылка / IP / etc.</th>
                        <th>Логин</th>
                        <th>Пароль</th>
                        <th>Инфомация</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="password__empty-msg" {if !empty($passwords)}style="display: none"{/if}>
                        <td colspan="5" class="text-center">Ничего не найдено</td>
                    </tr>
                    {if !empty($passwords)}
                        {foreach from=$passwords item="row"}
                            {include 'parts/_password_tr.tpl'}
                        {/foreach}
                    {/if}
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="5" class="text-right">
                            <a href="#" class="btn btn-outline-danger password__button-save"><i class="fa fa-save"></i></a>
                            <a href="#" class="btn btn-outline-success password__button-add"
                               data-target=".password__table tbody"><i class="fa fa-plus"></i></a>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </form>
        </div>
    </div>
</div>

{include 'parts/_footer.tpl'}


