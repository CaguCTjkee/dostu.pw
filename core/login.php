<?php
/**
 * Created by PhpStorm.
 * User: CaguCT
 * Date: 1/14/18
 * Time: 15:59
 */

namespace Core;

class Login {

	private $isLogin = false;
	private $sessionExp = 300;
	public $config;

	public function __construct()
	{
		$this->config = new Config();
	}

	/**
	 * @return bool
	 */
	public function isLogin(): bool
	{
		if( !empty($_SESSION['isLogin']) && $_SESSION['isLogin'] === true )
		{
			$this->isLogin = $_SESSION['isLogin'];
		}

		$this->updateLoginExp();

		return $this->isLogin;
	}

	public function updateLoginExp()
	{
		// Check login exp
		if( $this->isLogin )
		{
			if( $_SESSION['isLoginExp'] < time() )
			{
				$this->logout();
			}
			else
			{
				$_SESSION['isLoginExp'] = time() + $this->sessionExp;
			}
		}
	}

	/**
	 * @param string $password
	 *
	 * @return bool
	 */
	public function loginByPassword($password)
	{
		if( !empty($password) )
		{
			if( md5(md5($password)) === $this->config->getPassword() )
			{
				$this->isLogin          = true;
				$_SESSION['isLogin']    = $this->isLogin;
				$_SESSION['isLoginExp'] = time() + $this->sessionExp;

				return $this->isLogin;
			}
		}

		return false;
	}

	public function logout()
	{
		$this->isLogin          = false;
		$_SESSION['isLogin']    = $this->isLogin;
		$_SESSION['isLoginExp'] = $this->isLogin;
	}
}