<?php
/**
 * Created by PhpStorm.
 * User: CaguCT
 * Date: 1/11/18
 * Time: 10:02
 */

namespace Core;

/**
 * Class Controller
 * @package Core
 */
class Controller {

	/**
	 * @var array
	 */
	public $requestArray = [];
	/**
	 * @var string
	 */
	public $requestUrl;
	/**
	 * @var array
	 */
	public $paths = [];
	/**
	 * @var Config
	 */
	public $config;
	/**
	 * @var Login
	 */
	public $login;

	/**
	 * Controller constructor.
	 */
	public function __construct()
	{
		$this->config = new Config();
		$this->login  = new Login();

		if( !empty($_GET) )
		{
			$this->requestUrl   = http_build_query($_GET);
			$this->requestArray = $_GET;
		}

		// Test for logged user
		if( $this->login->isLogin() === false )
		{
			// Try to login
			if( !empty($_POST['password']) )
			{
				if( $this->login->loginByPassword($_POST['password']) )
				{
					Request::redirect('/');
				}
			}

			// View login template
			if( $this->login->isLogin() === false )
			{
				return $this->login();
			}
		}

		// Try to find function of this class
		if( !empty($this->requestArray['do']) && method_exists($this, $this->requestArray['do']) )
		{
			return call_user_func([$this, $this->requestArray['do']]);
		}
		else
		{
			return $this->index();
		}
	}

	/**
	 * Logout page /index.php?do=logout
	 */
	public function logout()
	{
		if( $this->login->isLogin() )
		{
			$this->login->logout();
		}

		Request::redirect('/');
	}

	/**
	 * Login page
	 *
	 * @throws \Exception
	 * @throws \SmartyException
	 */
	public function login()
	{
		return View::getInstance()->smarty->display('login.tpl');
	}

	/**
	 * Index page
	 *
	 * @throws \Exception
	 * @throws \SmartyException
	 */
	public function index()
	{
		$smarty = View::getInstance()->smarty;

		// Get info
		$assign = [
			'title'     => 'Main',
			'passwords' => Model::getInstance()->select('passwords'),
		];

		// Set vars
		$smarty->assign($assign);

		// Display index
		return $smarty->display('index.tpl');
	}

	/**
	 * SaveData page (used for ajax)
	 * This function save passwords to database
	 */
	public function saveData()
	{
		if( $this->login->isLogin() )
		{
			$this->login->updateLoginExp();

			$result = [];
			$data   = null;

			// Get post data
			foreach( $_POST['title'] as $id => $item )
			{
				$data[$id] = [
					'title'       => !empty($_POST['title'][$id]) ? $_POST['title'][$id] : null,
					'link'        => !empty($_POST['link'][$id]) ? $_POST['link'][$id] : null,
					'login'       => !empty($_POST['login'][$id]) ? $_POST['login'][$id] : null,
					'password'    => !empty($_POST['password'][$id]) ? $_POST['password'][$id] : null,
					'description' => !empty($_POST['description'][$id]) ? $_POST['description'][$id] : null,
				];
			}

			Model::getInstance()->insert('passwords', $data);
		}
		else
		{
			$result['error'] = 'You not logged';
		}
		echo json_encode($result);
	}

	/**
	 * Return template part from view/ folder (used for ajax)
	 *
	 * @param null $data
	 * @param null $template
	 *
	 * @throws \Exception
	 * @throws \SmartyException
	 */
	public function getTemplatePart($data = null, $template = null)
	{
		if( $this->login->isLogin() )
		{
			$this->login->updateLoginExp();

			$data     = !empty($_POST['data']) ? $_POST['data'] : $data;
			$template = !empty($_GET['template']) ? $_GET['template'] : $template;
			$smarty   = View::getInstance()->smarty;

			if( $data !== null )
			{
				$smarty->assign($data);
			}

			if( $template !== null )
			{
				if( is_file(ROOT . DS . TEMPLATE_DIRNAME . DS . $template) )
				{
					$smarty->display($template);
				}
				else
				{
					echo '$template: ' . $template . ' not found';
				}
			}
			else
			{
				echo '$template not set';
			}
		}
		else
		{
			echo 'You not logged';
		}
	}
}