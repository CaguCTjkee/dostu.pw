<?php

define('FILE', __FILE__);
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath(__DIR__ . DS . '..'));
define('HOST', 'http://dostup.lc');
define('TEMPLATE_DIRNAME', 'view');
define('TEMPLATE_DIR', ROOT . DS . TEMPLATE_DIRNAME);
define('PUBLIC_DIRNAME', 'public');
define('PUBLIC_DIR', ROOT . DS . PUBLIC_DIRNAME);
define('ASSETS', PUBLIC_DIR . DS . 'assets');

// Start the session
session_start();

// Include autoload
include_once ROOT . DS . 'core' . DS . 'autoload.php';

// Main controller
new \Core\Controller();